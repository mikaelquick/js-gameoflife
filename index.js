$(document).ready(function () {
    $(".theGame").height($(window).height() * 0.90);

    $("#panel1").height($(window).height() * 0.90);
    $("#panel1").width($(window).width() * 0.10);

    var click = 0;
    gridArray = [];
    gameIsLive = true;
    var height = 10;
    var width = 10;
    var cellXsize = 10;
    var cellYsize = 10;
    var nextGen;
    drawGrid();

    function drawGrid()
    {
        var boardXsize = $(".theGame").width();
        var boardYsize = $(".theGame").height();
        cellXsize = (boardXsize / width) / boardXsize * 100;
        cellYsize = (boardYsize / height) / boardYsize * 100;

        for (y = 0; y < height; y++) {

            gridArray[y] = [];

            for (x = 0; x < width; x++) {
                gridArray[y][x] = $('<img class="cell" style="width:' + cellXsize + '%;height:' + cellYsize + '%"/>').appendTo(".theGame");
    
            }
        }
    }

    $(".randomKnapp").click(function () {
        giveRandomLife();
    });

    $("#dropButtom").click(function () {
        $("#dropDownMenu").height(280);
        $("#dropDownMenu").slideToggle();
    });

    $(window).resize(function () {

        $(".theGame").height($(window).height() * 0.90);
        $("#panel1").height($(".theGame").height());
        $("#panel1").width($(window).width() * 0.10);
        $("#dropDownMenu").hide();
    
    });
  
    function giveRandomLife() {
        var maxValue_Y = gridArray.length;
        var maxValue_X = gridArray[0].length;
        var amount = Math.floor((maxValue_X + maxValue_Y) / 3);

        for (i = 0; i <= amount; i++) {
            giveLife(gridArray[Math.floor((Math.random() * maxValue_Y) + 0)][Math.floor((Math.random() * maxValue_X) + 0)]);
        }
    }

    function giveLife(cell) {
        cell.attr("src", "spider.svg");
    }

    $('#widthBox').keypress(function (e) {
        if (e.which == 13)  
        {
            width = $("#widthBox").val();
            height = $("#boxHeight").val();
            $(".theGame").empty();
            drawGrid();
        }
    });

    $('#boxHeight').keypress(function (e) {
        if (e.which == 13) 
        {
            width = $("#widthBox").val();
            height = $("#boxHeight").val();
            $(".theGame").empty();
            drawGrid();
        }
    });

    $('#dropWidth').keypress(function (e) {
        if (e.which == 13) {
            width = $("#dropWidth").val();
            height = $("#dropHeight").val();
            $(".theGame").empty();
            drawGrid();
        }
    });

    $('#dropHeight').keypress(function (e) {
        if (e.which == 13) {
            width = $("#dropWidth").val();
            height = $("#dropHeight").val();
            $(".theGame").empty();
            drawGrid();
        }
    });

    function takeLife(cell) {
        cell.attr("src", "dead.svg");
    }

    function isAlive(cell) {

        if (cell.attr("src") == "spider.svg")
            return true;

        return false;
    }

    //Decider of life and death
    function GOD(cell)
    {
        if (isAlive(cell))
            takeLife(cell);
        else
            giveLife(cell);
    }

    //when cells missbehave
    function killAll()
    {
        for (y = 0; y < height; y++)
            for (x = 0; x < width; x++)
                takeLife(gridArray[y][x]);
    }

    $('body').on('click', '.cell', function ()
    {
        if (isAlive($(this)))
            takeLife($(this));
        else
            giveLife($(this));
    });

    $("#clearButton").click(function () {
        killAll();
    });


    $(".bild1").click(function ()
    {
        click++;
        if (click == 1)
        {
            $(".bild1").attr("src", "pause.svg");
            gameIsLive = true;
            $("#dropHeight").prop("disabled", true);
            $("#dropWidth").prop("disabled", true);
            $("#boxHeight").prop("disabled", true);
            $("#widthBox").prop("disabled", true);
            $("#dropDownMenu").hide();
            start();
        }
        if (click == 2)
        {
            $(".bild1").attr("src", "play.svg");
            $("#widthBox").prop("disabled", false);
            $("#boxHeight").prop("disabled", false);
            $("#dropHeight").prop("disabled", false);
            $("#dropWidth").prop("disabled", false);
            gameIsLive = false;
            click = 0;
        }
    });


    function countNeigh(cell)
    {
        var neigbors = 0;

        //down
        if (gridArray.length - 1 > y && isAlive(gridArray[y + 1][x]))
            neigbors++;

        //up
        if (y > 0 && isAlive(gridArray[y - 1][x]))
            neigbors++;

        //left
        if (x > 0 && isAlive(gridArray[y][x - 1]))
            neigbors++;

        //right
        if (gridArray[y].length - 1 > x && isAlive(gridArray[y][x + 1]))
            neigbors++;

        //downRight
        if (gridArray.length - 1 > y && gridArray[y].length - 1 > x && isAlive(gridArray[y + 1][x + 1]))
            neigbors++;

        //downLeft
        if (gridArray.length - 1 > y && x > 0 && isAlive(gridArray[y + 1][x - 1]))
            neigbors++;

        //upLeft
        if (y > 0 && x > 0 && isAlive(gridArray[y - 1][x - 1]))
            neigbors++;

        //upRight
        if (y > 0 && gridArray[y].length - 1 > x && isAlive(gridArray[y - 1][x + 1]))
            neigbors++;

        return neigbors;
    }

    function nextStep()
    {
        nextGen = [];

        for (y = 0; y < gridArray.length; y++)
        {
            for (x = 0; x < gridArray[y].length; x++)
            {
                var neigbors = countNeigh(gridArray[y][x]);
                var theCell = gridArray[y][x];

                //less then 2 is death
                if (isAlive(gridArray[y][x]) && neigbors < 2)
                    nextGen.push(gridArray[y][x]);

                //3 neighbors creates life
                if (!isAlive(theCell) && neigbors === 3)
                    nextGen.push(theCell);

                //more then 3 neighbors is death
                if (neigbors > 3 && isAlive(gridArray[y][x]))
                    nextGen.push(theCell);
            }
        }

        //God decides the faith of the cells
        for (i = 0; i < nextGen.length; i++)
            GOD(nextGen[i]);
    }

    function start(){
        if (gameIsLive)
        {
            nextStep();
            setTimeout(start, speedChecker());
        }
    }

    function speedChecker(){
        if ($("#fastRadio").is(":checked") || $("#dropFast").is(":checked"))
            return 100;
        else if ($("#slowRadio").is(":checked") || $("#dropSlow").is(":checked"))
            return 500;
        else if ($("#normalRadio").is(":checked") || $("#dropNormal").is(":checked"))
            return 300;

        return 300;
    }

});





